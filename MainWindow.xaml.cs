﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GBImageResizer {
    /// <summary>
    /// Interaction logic for ImageResampler.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private InterfaceBackend ui;

        public MainWindow() {
            InitializeComponent();
            ui = new InterfaceBackend(this);
            this.DataContext = ui;
        }

        
        protected override void OnDrop(DragEventArgs e) {
            base.OnDrop(e);
            gridImages_Drop(this, e);
            e.Handled = true;
        }

        private void gridImages_Drop(object sender, DragEventArgs e) {
            try {
                ui.processFileDrop(e);
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Error Dropping Files", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Button_Stop_Click(object sender, RoutedEventArgs e) {
            ui.ShouldStop = true;
        }

        private void button_BrowseFolder_Click(object sender, RoutedEventArgs e) {
            System.Windows.Forms.FolderBrowserDialog openFolder = new System.Windows.Forms.FolderBrowserDialog();
            openFolder.Description = "Select folder where to save new resampled images";
            openFolder.ShowNewFolderButton = true;
            System.Windows.Forms.DialogResult res = openFolder.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK) { 
                textNewFolder.Text = openFolder.SelectedPath;
            }            
        }

        private void Button_Close_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            ui.ShouldStop = true;
            Application.Current.Shutdown(0);
        }

        private void Button_Start_Click(object sender, RoutedEventArgs e) {
            ui.startResampling();
        }

        private void RowDoubleClick(object sender, RoutedEventArgs e) {
            var row = sender as DataGridRow;
            row.DetailsVisibility = row.DetailsVisibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
        }

        private void Button_Clear_Click(object sender, RoutedEventArgs e) {
            ui.clearList();
        }

        private void Button_Remove_Click(object sender, RoutedEventArgs e) {
            ui.removeSelectedItems();
        }

        private void RowSelected(object sender, SelectionChangedEventArgs e) {
            ui.RemoveListEnabled = true;
        }

        
        private void Radio_Output_changed(object sender, RoutedEventArgs e) {
            if(ui != null) ui.ControlsEnabled = true;
        }
    }
}
