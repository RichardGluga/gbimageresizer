﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBImageResizer {
    public class ImageRecord : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName) {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        } 

        private string filename;

        public string Filename {
            get { return filename; }
            set { filename = value; OnPropertyChanged("Filename"); }
        }
        private string folder;

        public string Folder {
            get { return folder; }
            set { folder = value; OnPropertyChanged("Folder"); }
        }
        private DateTime created;

        public DateTime Created {
            get { return created; }
            set { created = value; OnPropertyChanged("Created"); }
        }
        private long size;

        public long Size {
            get { return size; }
            set { size = value; OnPropertyChanged("Size"); }
        }
        private string status;

        public string Status {
            get { return status; }
            set { status = value; OnPropertyChanged("Status"); }
        }

        private int width;

        public int Width {
            get { return width; }
            set { width = value; OnPropertyChanged("Width"); }
        }
        private int height;

        public int Height {
            get { return height; }
            set { height = value; OnPropertyChanged("Height"); }
        }

        private bool resampleOk = false;

        public bool ResampleOk {
            get { return resampleOk; }
            set { resampleOk = value; OnPropertyChanged("ResampleOk"); }
        }
        private bool resampleError = false;

        public bool ResampleError {
            get { return resampleError; }
            set { resampleError = value; OnPropertyChanged("ResampleError"); }
        }

        public override string ToString() {
            string str = "";
            str += "Filename: " + Folder + "\\" + Filename + "\n";
            str += "Size: " + Size + " KB\n";
            str += "Resolution: " + Width + " x " + Height + "\n";
            str += "Status: " + Status;
            return str;
        }
    }
}
