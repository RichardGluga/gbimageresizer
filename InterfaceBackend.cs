﻿using gb.util;
using GBUtils;
using SimpleLogger;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace GBImageResizer {
    public class InterfaceBackend : INotifyPropertyChanged, IDisposable {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName) {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private MainWindow parent;

        private bool includeSubfolders = false;
        public bool IncludeSubfolders {
            get { return includeSubfolders; }
            set { includeSubfolders = value; OnPropertyChanged("IncludeSubfolders"); }
        }

        public bool ClearListEnabled {
            get { return ControlsEnabled && parent.gridImages.Items.Count > 0; }
            set { OnPropertyChanged("ClearListEnabled"); }
        }

        public bool RemoveListEnabled {
            get { return ControlsEnabled && parent.gridImages.SelectedItems.Count > 0; }
            set { OnPropertyChanged("RemoveListEnabled"); }
        }

        private bool controlsEnabled = true;
        public bool ControlsEnabled {
            get { return controlsEnabled; }
            set { 
                controlsEnabled = value; 
                OnPropertyChanged("ControlsEnabled"); 
                OnPropertyChanged("IsBusy");
                OnPropertyChanged("ClearListEnabled");
                OnPropertyChanged("RemoveListEnabled");
                OnPropertyChanged("RadioPrefixEnabled");
                OnPropertyChanged("NewFolderEnabled");
            }
        }

        public bool IsBusy {
            get { return controlsEnabled == false; }
            set { OnPropertyChanged("ControlsEnabled"); }
        }

        public bool RadioPrefixEnabled {
            get { return ControlsEnabled && parent.radioPrefix.IsChecked == true; }
            set { OnPropertyChanged("RadioPrefixEnabled"); }
        }

        public bool NewFolderEnabled {
            get { return ControlsEnabled && parent.radioNewFolder.IsChecked == true; }
            set { OnPropertyChanged("NewFolderEnabled"); }
        }

        private ObservableCollection<ImageRecord> imageRecords = new ObservableCollection<ImageRecord>();
        private ObservableCollection<string> codecs = new ObservableCollection<string>();

        public ObservableCollection<string> Codecs {
            get { return codecs; }
            set { codecs = value; OnPropertyChanged("Codecs"); }
        }

        public ObservableCollection<ImageRecord> ImageRecords {
            get { return imageRecords; }
            set { imageRecords = value; OnPropertyChanged("ImageRecords"); }
        }
        private bool shouldStop = false;

        public bool ShouldStop {
            get { return shouldStop; }
            set { 
                shouldStop = value; 
                OnPropertyChanged("ShouldStop");
                 if (value == true && sourceToken != null) {
                    sourceToken.Cancel();
                }
            }
        }
        private int progressMin = 0;
        public int ProgressMin {
            get { return progressMin; }
            set { progressMin = value; OnPropertyChanged("ProgressMin"); }
        }
        private int progressMax = 100;
        public int ProgressMax {
            get { return progressMax; }
            set { progressMax = value; OnPropertyChanged("ProgressMax"); }
        }
        private int progressValue = 0;
        public int ProgressValue {
            get { return progressValue; }
            set { progressValue = value; OnPropertyChanged("ProgressValue"); }
        }
        private string statusText = "Ready...";
        public string StatusText {
            get { return statusText; }
            set { statusText = value; OnPropertyChanged("StatusText"); }
        }

        private int maxWidth = 1920;

        public int MaxWidth {
            get { return maxWidth; }
            set { maxWidth = value; OnPropertyChanged("MaxWidth"); }
        }
        private int maxHeight = 1080;

        public int MaxHeight {
            get { return maxHeight; }
            set { maxHeight = value; OnPropertyChanged("MaxHeight"); }
        }
        private int quality = 85;

        public int Quality {
            get { return quality; }
            set { quality = value; OnPropertyChanged("Quality"); }
        }

        private bool crop = false;

        public bool Crop {
            get { return crop; }
            set { crop = value; OnPropertyChanged("Crop"); }
        }

        private int codecSelectedIndex = 0;

        public int CodecSelectedIndex {
            get { return codecSelectedIndex; }
            set { codecSelectedIndex = value; OnPropertyChanged("CodecSelectedIndex"); }
        }

        private string codecSelectedValue = "";

        public string CodecSelectedValue {
            get { return codecSelectedValue; }
            set { codecSelectedValue = value; OnPropertyChanged("CodecSelectedValue"); }
        }


        CancellationTokenSource sourceToken;

        public InterfaceBackend(MainWindow parent) {
            this.parent = parent;
            ImageRecords.CollectionChanged += ImageRecords_CollectionChanged;

            //initialize the output encoders dropdown and select jpg/jpeg as the default (if available)
            int index = 0;
            foreach (string ext in WpfImageUtil.listEncoderExtensions()) {
                Codecs.Add(ext);
                if (ext.Equals("jpg") || ext.Equals("jpeg")) {
                    CodecSelectedIndex = index;
                    CodecSelectedValue = ext;
                }
                index++;
            }
        }

        void ImageRecords_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
            StatusText = ImageRecords.Count + " files left in queue";
        }

        public void clearList() {
            ImageRecords.Clear();
        }

        public void removeSelectedItems() {
            List<ImageRecord> toRemove = new List<ImageRecord>();
            foreach (ImageRecord ir in parent.gridImages.SelectedItems) toRemove.Add(ir);
            foreach (ImageRecord ir in toRemove) ImageRecords.Remove(ir);
        }

        public void processFileDrop(DragEventArgs e) {
             //initialize a default task scheduler and get a handle to a cancellation token
            TaskScheduler scheduler = TaskScheduler.Default;
            sourceToken = new CancellationTokenSource();
            Task asyncTask = new Task(() => {
                Array a = (Array)e.Data.GetData(DataFormats.FileDrop);
                if (a != null) {
                    Task task = new Task(async() => {
                        ShouldStop = false;
                        ControlsEnabled = false;

                        ProgressMin = 0;
                        StatusText = "Processing dropped files...";

                        ProgressMax = a.Length;
                        List<Task> tasks = new List<Task>();
                        foreach (object fo in a) {
                            if (ShouldStop) break;
                            String filename = fo.ToString();
                            if (Directory.Exists(filename)) tasks.AddRange(addFilesFromFolder(filename, true));
                            else tasks.Add(addFile(filename));
                        }

                        try {
                            await  Task.WhenAll(tasks.ToArray());
                            StatusText = ImageRecords.Count + " files in queue.";
                        } catch (TaskCanceledException ex) {
                            StatusText = ImageRecords.Count + " files in queue.";
                        }

                        ProgressValue = 0;
                        ShouldStop = false;
                        ControlsEnabled = true;
                    }, sourceToken.Token);
                    task.Start();
                }
            },sourceToken.Token);
            asyncTask.Start();

            //worker = new BackgroundWorker();
            //worker.WorkerSupportsCancellation = true;
            //worker.DoWork += new DoWorkEventHandler(
            //    delegate(object o, DoWorkEventArgs args) {
                    
            //    }
            //);
            //worker.RunWorkerAsync();
        }

        private Task addFile(string filename) {
            Task task = new Task(() => {
                try {
                    FileInfo fi = new FileInfo(filename);
                    if (WpfImageUtil.hasDecoderByExt(fi.Extension)) {
                        ImageRecord ir = new ImageRecord {
                            Filename = fi.Name,
                            Folder = fi.DirectoryName,
                            Created = fi.CreationTime,
                            Size = fi.Length > 0 ? fi.Length / 1024 : 0,
                            Status = "Queued..."
                        };

                        try {
                            System.Drawing.Point p = GBUtils.WpfImageUtil.getImageDimensions(filename);
                            ir.Width = p.X;
                            ir.Height = p.Y;

                            Application.Current.Dispatcher.Invoke((Action)(() => {
                                ImageRecords.Add(ir);
                                ProgressValue += 1;
                                StatusText = ImageRecords.Count + " files added...";
                            }));
                        } catch (Exception ex) {
                            //
                        }
                    }
                } catch (Exception ex) { 
                    //
                }
            }, sourceToken.Token);
            task.Start();
            return task;
        }

        private Task[] addFilesFromFolder(string foldername, bool topLevel = false) {
            List<Task> tasks = new List<Task>();

            try {
                string[] files = Directory.GetFiles(foldername);
                ProgressMax += files.Length;
                foreach (string filename in files) tasks.Add(addFile(filename));

                if (IncludeSubfolders) {
                    string[] folders = Directory.GetDirectories(foldername);
                    foreach (string folder in folders) tasks.AddRange(addFilesFromFolder(folder).ToArray());
                }
            } catch (Exception ex) { 
                // could not open folder
            }

            return tasks.ToArray();
        }




        public void startResampling() {
            //initialize necessary parameters and settings
            String format = CodecSelectedValue;
            String f_path = null;
            String f_prefix = "";
            bool overwrite = false;

            if (parent.radioOverwrite.IsChecked == true) overwrite = true;
            if (parent.radioPrefix.IsChecked == true) f_prefix = parent.textPrefix.Text.Trim();
            if (parent.radioNewFolder.IsChecked == true) {
                f_path = parent.textNewFolder.Text + "\\";
                if (!Directory.Exists(f_path)) {
                    StatusText = "ERROR: Output folder does not exist!";
                }
                
            }
            String f_ext = "." + format;

            //initialize a default task scheduler and get a handle to a cancellation token
            TaskScheduler scheduler = TaskScheduler.Default;
            sourceToken = new CancellationTokenSource();

            //create a single task that will be executed asynchronously, outside the UI thread
            //this task will be the controller of all sub-tasks, handling all the orchestration
            Task asyncTask = new Task(() => {
                //disable the interface controls
                ShouldStop = false;
                ControlsEnabled = false;

                int errors = 0;
                int ok = 0;
                for (int i = 0; i < ImageRecords.Count; i++) {
                    int ii = i;
                    if (ShouldStop) break;    
                    //scroll the image grid to the row representating image being resampled, note this must be done in main UI thread
                    Application.Current.Dispatcher.Invoke((Action)(() => {
                        parent.gridImages.ScrollIntoView(ImageRecords[ii]);
                    }));

                    //skip over files we already tried to resample
                    if (ImageRecords[ii].ResampleOk == true) return;      
                    String filename = ImageRecords[ii].Folder + "\\" + ImageRecords[ii].Filename;

                    try {
                        //get the file and name
                        FileInfo fi = new FileInfo(filename);
                        BitmapSource image = WpfImageUtil.loadBitmapSource(filename);
                        String f_name = Path.GetFileNameWithoutExtension(fi.FullName);

                        //do the resampling in memory
                        if (Crop == true) image = WpfImageUtil.resize_and_crop(image, MaxWidth, MaxHeight);
                        else image = WpfImageUtil.resize_maintain_aspect_max(image, MaxWidth, MaxHeight);

                        //save to correct destination
                        if (f_path == null) f_path = fi.Directory.FullName + "\\";
                        String saveto = f_path + f_prefix + f_name + f_ext;

                        //copy over the original creation time
                        WpfImageUtil.saveImage(image, saveto, format, quality);
                        FileInfo newFile = new FileInfo(saveto);
                        newFile.CreationTime = fi.CreationTime;

                        int newWidth = image.PixelWidth;
                        int newHeight = image.PixelHeight;

                        //update the information for this image
                        Application.Current.Dispatcher.Invoke((Action)(() => {
                            ok++;
                            ImageRecords[ii].Status = "OK!";
                            ImageRecords[ii].ResampleOk = true;
                            ImageRecords[ii].ResampleError = false;

                            ImageRecords[ii].Filename = newFile.Name;
                            ImageRecords[ii].Folder = newFile.Directory.FullName;
                            ImageRecords[ii].Size = newFile.Length / 1024;
                            ImageRecords[ii].Width = newWidth;
                            ImageRecords[ii].Height = newHeight;
                        }));

                        if (overwrite && !saveto.Trim().Equals(fi.FullName.Trim())) {
                            //may need to delete the original... but should we? or just leave it? maybe safer to leave it
                        }

                    } catch (Exception ex) {
                        //image failed to resize, so leave it in the grid, and provide a reason
                        Application.Current.Dispatcher.Invoke((Action)(() => {
                            errors++;
                            ImageRecords[ii].Status = "Error: " + ex.Message;
                            ImageRecords[ii].ResampleOk = false;
                            ImageRecords[ii].ResampleError = true;
                        }));
                    }

                    ProgressMin = 0;
                    ProgressValue = ProgressValue + 1;
                    ProgressMax = ImageRecords.Count;
                    StatusText = "Resampling... (" + ProgressValue + " of " + ImageRecords.Count + " OK, " + errors + " errors)";
                }

                if (ShouldStop == false) {
                    StatusText = "Done! (" + ProgressValue + " of " + ImageRecords.Count + " OK, " + errors + " errors " + (errors > 0 ? " -- click Start to retry failed files" : "") + ")";
                } else {
                    StatusText = "Stopped (click Start to resume)";
                }

                ProgressMin = 0;
                ProgressValue = 0;
                ProgressMax = 100;

                ShouldStop = false;
                ControlsEnabled = true;
                sourceToken = null;
            }, sourceToken.Token, TaskCreationOptions.None);
            asyncTask.Start(scheduler);
        }

        public void Dispose() {
            throw new NotImplementedException();
        }
    }
}
